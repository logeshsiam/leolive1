import React from "react";
import { Text, StyleSheet, View, Image, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import EIcon from 'react-native-vector-icons/Entypo';
import { SafeAreaView } from "react-native-safe-area-context";

import { RadioButton } from 'react-native-paper';

const Login1 = () => {
  const [text, onChangeText] = React.useState(null);
  const [text1, onChangeText1] = React.useState(null);
  const [text2, onChangeText2] = React.useState(null);
  const [text3, onChangeText3] = React.useState(null);
  const [text4, onChangeText4] = React.useState(null);
  const [text5, onChangeText5] = React.useState(null);
  const [text6, onChangeText6] = React.useState(null);
  const [text7, onChangeText7] = React.useState(null);
  const [text8, onChangeText8] = React.useState(null);
  const [text9, onChangeText9] = React.useState(null);
  const [text10, onChangeText10] = React.useState(null);
  const [text11, onChangeText11] = React.useState(null);

  const [checked, setChecked] = React.useState('first');

  return (

    <ScrollView style={{ flexDirection: 'column' }}>

      <SafeAreaView>

        <Image source={require('../assets/k1.png')} style={styles.k1} />
        <EIcon name="chevron-small-left" size={19} color="#ffffff" style={{ fontSize: 45, position: "absolute", marginLeft: 20, marginTop: 45 }} />
        <Text style={styles.backtext}>  Back </Text>
        <LinearGradient colors={['#57508D', '#F54BA1']} style={styles.linearGradient}></LinearGradient>

        <Text style={styles.text1}>Tell us more about yourself </Text>

        <Text style={styles.text2}>We will need some basic information about you {'\n'} before we get started.</Text>

        <Image source={require('../assets/group1.png')} style={styles.k2} />

        <Text style={styles.text3}>Who is your current employer (please {'\n'}provide exact company name)?</Text>

        <TextInput
          style={styles.input1}
          onChangeText={onChangeText}
          value={text}
          placeholder="Max 100 Characters"
        />

        <Text style={styles.text4}>Sector/Industry</Text>

        <TextInput
          style={styles.input2}
          onChangeText={onChangeText1}
          value={text1}
          placeholder="Select"
        />

        <Text style={styles.text4}>Working Duration (Tenure in Years)</Text>

        <TextInput
          style={styles.input3left}
          onChangeText={onChangeText2}
          value={text2}
          placeholder="Years"
        />

        <TextInput
          style={styles.input3right}
          onChangeText={onChangeText3}
          value={text3}
          placeholder="Months"
        />

        <Text style={styles.text4}>Position/Job title</Text>

        <TextInput
          style={styles.input2}
          onChangeText={onChangeText4}
          value={text4}
          placeholder="Limit upto 100 characters"
        />

        <Text style={styles.text4}>Salary (RM per month before tax)</Text>

        <TextInput
          style={styles.input2}
          onChangeText={onChangeText5}
          value={text5}
          placeholder="Salary (RM per month before tax)"
        />

        <Text style={styles.text4}>Highest Academic Qualification</Text>

        <TextInput
          style={styles.input2}
          onChangeText={onChangeText6}
          value={text6}
          placeholder="Select"
        />

        <Text style={styles.text4}>Highest Institution Country</Text>


        <View style={{ left: 30, top: 5 }}>

          <RadioButton
            value="1"
            status={checked === '1' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('1')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>

        <Text style={styles.circletext1}>Malaysia</Text>

        <View style={{ left: 190, bottom: 50 }}>
          <RadioButton
            value="2"
            status={checked === '2' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('2')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>

        <Text style={styles.circletext2}>Other Countries</Text>

        <TextInput
          style={styles.input21}
          onChangeText={onChangeText7}
          value={text7}
          placeholder="Name Of Institution"
        />

        <TextInput
          style={styles.input22}
          onChangeText={onChangeText8}
          value={text8}
          placeholder="If Other Countries, select country"
        />
        <Text style={styles.text442}>Scope of study</Text>

        <TextInput
          style={styles.input222}
          onChangeText={onChangeText9}
          value={text9}
          placeholder="Salary (RM per month before tax)"
        />

        <Text style={styles.text4}>Please indicate your current grade</Text>

        <View style={{ left: 30, top: 5 }}>
          <RadioButton
            value="3"
            status={checked === '3' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('3')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 65, bottom: 20, color: '#000' }} >CGPA</Text>



        <View style={{ left: 140, bottom: 48 }}>
          <RadioButton
            value="4"
            status={checked === '4' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('4')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 175, bottom: 75, color: '#000', }}>Grades</Text>

        <View style={{ left: 250, bottom: 103 }}>
          <RadioButton
            value="5"
            status={checked === '5' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('5')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 285, bottom: 130, color: '#000', }}>Others</Text>

        <TextInput
          style={styles.input223}
          onChangeText={onChangeText10}
          value={text10}
          placeholder="Please enter"
        />

        <Text style={styles.text443}>English Equivalent Tests</Text>


        <View style={{ left: 35, bottom: 60 }}>
          <RadioButton
            value="6"
            status={checked === '6' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('6')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 75, bottom: 85, color: '#000', }} >MUET</Text>


        <View style={{ left: 35, bottom: 80 }}>
          <RadioButton
            value="7"
            status={checked === '7' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('7')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 75, bottom: 105, color: '#000', }} >CEFR</Text>
        <View style={{ left: 35, bottom: 95 }}>
          <RadioButton
            value="8"
            status={checked === '8' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('8')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 75, bottom: 120, color: '#000', }} >TOEFL</Text>
        <View style={{ left: 35, bottom: 110 }}>
          <RadioButton
            value="9"
            status={checked === '9' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('9')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 75, bottom: 135, color: '#000', }} >IELTS</Text>
        <View style={{ left: 35, bottom: 125 }}>
          <RadioButton
            value="10"
            status={checked === '10' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('10')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 75, bottom: 150, color: '#000', }} >Others</Text>
        <View style={{ left: 35, bottom: 135 }}>
          <RadioButton
            value="11"
            status={checked === '11' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('11')}
            color={'#FF69B4'}
            uncheckedColor={'#FF1493'}
          />
        </View>
        <Text style={styles.circletext2} style={{ left: 75, bottom: 165, color: '#000', }} >I have not taken any test</Text>

        <TextInput
          style={styles.input221}
          onChangeText={onChangeText11}
          value={text11}
          placeholder="Please enter"
        />

        <TouchableOpacity>
          <View style={{ marginTop: 30, bottom: 100, right: 10 }}>

            <LinearGradient colors={['#57508D', '#F54BA1']} style={styles.linearGradient1}>
              <Text style={styles.lasttext1}>
                Continue
              </Text>
            </LinearGradient>
          </View>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.textDesign2}>Save and Exit</Text>
        </TouchableOpacity>

        <View style={{ marginTop: 10 }}></View>

      </SafeAreaView>
    </ScrollView>



  )
}
export default Login1;

const styles = StyleSheet.create({
  container1: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  k1: {
    alignSelf: 'center',
    height: 250,
    width: 400,
    right: -4,
  },
  k2: {
    alignSelf: 'center',
    height: 50,
    width: 310,
    right: -4,
    marginTop: 30,
  },

  linearGradient1: {

    borderRadius: 128,
    height: 48,
    width: 134,
    marginLeft: 17,
    alignSelf: "center"
  },

  text1: {
    fontSize: 22,
    color: '#000',
    fontWeight: "400",
    alignSelf: 'center',
    marginTop: 20
  },

  circletext1: {
    left: 70,
    bottom: 22,
    // position: "absolute",
    fontSize: 14,
    color: '#000',
  },

  circletext2: {
    left: 230,
    // position: "absolute",
    bottom: 78,
    fontSize: 14,
    color: '#000',
  },

  text2: {
    fontSize: 12,
    color: '#808080',
    marginLeft: 60,
  },
  text3: {
    fontSize: 16,
    color: '#000',
    alignSelf: 'center',
    marginTop: 40,
  },

  text4: {
    fontSize: 16,
    color: '#000',
    left: 50,
    marginTop: 40,

  },
  text44: {
    fontSize: 16,
    color: '#000',
    left: 50,
    top: 10,
    marginTop: 40,

  },

  text442: {
    fontSize: 16,
    color: '#000',
    left: 50,
    bottom: 10,

  },

  text443: {
    fontSize: 16,
    color: '#000',
    left: 50,
    bottom: 70,

  },

  backtext: {
    fontSize: 25,
    position: "absolute",
    color: '#ffffff',
    marginLeft: 60,
    marginTop: 50
  },

  boxtext1: {
    marginTop: 50,
    left: -30,
    fontSize: 13,
    fontWeight: "bold",
    textAlign: 'right',
    color: '#ffffff',
  },


  lasttext1: {
    fontSize: 14,
    color: "#fff",
    marginTop: 15,
    alignSelf: "center",
  },

  textDesign2: {
    fontSize: 14,
    color: "#504F8C",
    bottom: 80,
    alignSelf: "center",
    textDecorationLine: "underline",
  },

  input1: {
    height: 48,
    width: 303,
    marginTop: 16,
    alignSelf: 'center',
    color: '#9A9A9A',
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,
  },
  input21: {
    height: 48,
    width: 303,
    bottom: 50,
    alignSelf: 'center',
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,

  },

  input22: {
    height: 48,
    width: 303,
    bottom: 30,
    alignSelf: 'center',
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,

  },


  input222: {
    height: 48,
    width: 303,
    top: 10,
    alignSelf: 'center',
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,

  },
  input223: {
    height: 48,
    width: 303,
    bottom: 110,
    alignSelf: 'center',
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,

  },

  input221: {
    height: 48,
    width: 303,
    bottom: 130,
    alignSelf: 'center',
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,

  },

  input2: {
    height: 48,
    width: 303,
    top: 10,
    marginTop: 15,
    alignSelf: 'center',
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,
  },
  input3left: {
    height: 48,
    width: 148,
    left: 31,
    marginTop: 15,
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,
  },

  input3right: {
    height: 48,
    width: 148,
    left: 200,
    position: "absolute",
    marginTop: 755,
    borderRadius: 128,
    backgroundColor: '#F3F3F3',
    borderWidth: 0.1,
    padding: 10,
    fontSize: 14,
  },

  linearGradient: {
    position: "absolute",
    borderRadius: 40,
    height: 250,
    width: 400,
    opacity: 0.3
  },

})