import React from "react";
import{
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import OTPInputView from '@twotalltotems/react-native-otp-input';


const OTP = ({navigation}) =>{
   
  return(
    <View>
    <View style= {{flexDirection:'row'}}>
      <Fontisto name="angle-left" size={14}  style={{marginStart: 6,marginTop: 30}}/>
      <Text style={{fontSize: 24, fontWeight:'500',marginTop: 20,marginStart: 8,color:'#000'}}>OTP Verification</Text>
    </View>
    <Text style={{marginTop:30,fontSize: 16,fontWeight:'400',marginStart: 30}}>Enter the OTP sent to your mobile number</Text>
    <View style={{alignSelf:'center',marginTop: 40}}>
     <OTPInputView
        pinCount={4}
        style={styles.otpView}
        codeInputFieldStyle={styles.underlineStyleBase}
        onCodeFilled={value => {
        console.log(value);
        }}
      />
      </View>
      <Text style={{alignSelf:'center',color:'#0095C6'}} >Resend OTP</Text>
       <TouchableOpacity style={{marginTop: 20}} onPress={() => navigation.navigate('Homepage')}>
        <Text style = {[styles.button,{color:'#fff',textAlign:'center'}]} >Verify</Text>
        </TouchableOpacity>
    </View>
  )
}
export default OTP;

const styles = StyleSheet.create({
  otpView: {
    width: '50%',
    height: 100,
    color: 'black',
  },
  underlineStyleBase: {
   alignSelf:'center',
   margin: 8,
   height:60,
   width:60,
   color: '#000',
   borderRadius:30,
   backgroundColor:'rgba(0, 149, 198, 0.24)'
  },
  button: {
      borderRadius: 5,
      borderWidth: 2,
      alignSelf:'center',
      fontSize: 18,
      height:40,
      width:200,
      padding: 6,
      backgroundColor:'#0095C6', 
      borderColor: '#0095C6', 
  },
});

