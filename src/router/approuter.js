import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';



import Hme from '../Hme';
import Login1 from '../Login1';
// import Home1 from '../Home1';
// import Slider from '../Slider';
// import PhoneNo from '../PhoneNo';
// import OTP from '../OTP';
// import Homepage from '../Homepage';

const RootStack = createNativeStackNavigator();
const Tab = createMaterialBottomTabNavigator();
function MyTabs() {
    return (
        <Tab.Navigator
            initialRouteName="Hme"
            activeColor="#e91e63"
            barStyle={{ backgroundColor: 'white' }}
        >
            <Tab.Screen
                name="Hme"
                component={Hme}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <Feather name="home" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Login1"
                component={Login1}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="briefcase-outline" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Home"
                component={Hme}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="page-next" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="login"
                component={Login1}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="bell-outline" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Home1"
                component={Hme}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <Feather name="user" color={color} size={26} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}
const approuter = () => {
    return (
        <NavigationContainer>

            <RootStack.Navigator>
                <RootStack.Screen name=" MyTabs" component={MyTabs} options={{ headerShown: false }} />
                <RootStack.Screen name="Login1" component={Login1} options={{ headerShown: false }} />
                 {/* <RootStack.Screen name="Home1" component={Home1} options={{headerShown: false}}/> 
                <RootStack.Screen name="Drawer" component={Drawer} options={{ headerShown: false }} />
                <RootStack.Screen name="Slider" component={Slider} options={{headerShown: false}}/>
                <RootStack.Screen name="PhoneNo" component={PhoneNo} options={{headerShown: false}} />
                <RootStack.Screen name="OTP" component={OTP} options={{headerShown: false}} />
                <RootStack.Screen name="Homepage" component={Homepage} options={{headerShown: false}} /> */}
            </RootStack.Navigator>

        </NavigationContainer>
    )
}
export default approuter;